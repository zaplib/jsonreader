﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace JSONReader
{
    public class ArrayTuple : IJsonTuple
    {
        private List<IJsonTuple> data;

        public IJsonTuple this[int idx]
        {
            get => _get(idx);
            set => _set(idx, value);
        }
        public IJsonTuple this[string idx]
        {
            get => _get(Cast.To(idx, -1));
            set => _set(Cast.To(idx, -1), value);
        }

        private void _set(int idx, IJsonTuple value)
        {
            if (data == null || idx >= data.Count || idx < 0) return;
            data[idx] = value;
        }

        private IJsonTuple _get(int idx) => data == null || idx >= data.Count || idx < 0 ? new ValueTuple(null) : data[idx];

        public ArrayTuple(bool create_init_object = true)
        {
            if (create_init_object) data = new List<IJsonTuple>();
            Trace.WriteLine("Create Array");
        }

        public List<IJsonTuple> Value()
        {
            Trace.WriteLine("Append Array");
            return data;
        }

        public T Value<T>(T default_value) => Cast.To(data, default_value);

        public object Value(object default_value) => data ?? default_value;
    }
}
