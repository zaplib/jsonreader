﻿using System.Collections.Generic;
using System.Diagnostics;

namespace JSONReader
{
    public class ObjectTuple : IJsonTuple
    {
        private Dictionary<string, IJsonTuple> data;

        public IJsonTuple this[int idx]
        {
            get => _get(Cast.To(idx, "0"));
            set => _set(Cast.To(idx, "0"), value);
        }

        public IJsonTuple this[string idx]
        {
            get => _get(idx);
            set => _set(idx, value);
        }

        private void _set(string key, IJsonTuple val)
        {
            if (data == null || key==null) return;
            Trace.WriteLine($"Set: {key} Value: {val?.ToString()}");
            data[key] = val;
        }

        private IJsonTuple _get(string key) => data == null || !data.ContainsKey(key) ? new ValueTuple(null) : data[key];


        public ObjectTuple(bool create_init_object = true)
        {
            if (create_init_object) data = new Dictionary<string, IJsonTuple>();
            Trace.WriteLine("Create Object");
        }

        public T Value<T>(T default_value = default) => Cast.To(data, default_value);

        public object Value(object default_value = null) => data ?? default_value;
    }
}
