﻿using System.Diagnostics;

namespace JSONReader
{
    public class ValueTuple : IJsonTuple
    {
        private object value;
        public IJsonTuple this[int idx]
        {
            get => new ValueTuple(null);
            set { }
        }

        public IJsonTuple this[string idx]
        {
            get => new ValueTuple(null);
            set { }
        }

        public ValueTuple(object value)
        {
            this.value = value;
            Trace.WriteLine("Set value: " + value?.ToString());
        }
        public T Value<T>(T default_value) => Cast.To(value, default_value);
        public object Value(object default_value = null) => value;
    }
}
