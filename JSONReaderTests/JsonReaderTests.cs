﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.IO;

namespace JSONReader.Tests
{
    [TestClass()]
    public class JsonReaderTests
    {
        [TestMethod()]
        public void ParseNormalTest()
        {
            string json = File.ReadAllText("Test1.json"); 
            Trace.WriteLine(json);
            Trace.WriteLine("----------------------------");
            JsonReader r = new JsonReader();
            var data = r.Parse(json);
            Assert.IsNotNull(data); // can parse

            string val1 = data["info"]["logo"]["work"]["good"]["url"].Value<string>();
            string val2 = data["info"]["product"][0].Value<string>();
            Assert.AreEqual("http://www.google.com", val1); //http://www.google.com
            Assert.AreEqual("apple", val2); //apple
        }
        [TestMethod()]
        public void ParseArrayTest()
        {
            string json = File.ReadAllText("Test2.json");
            Trace.WriteLine(json);
            Trace.WriteLine("----------------------------");
            JsonReader r = new JsonReader();
            var data = r.Parse(json);
            Assert.IsNotNull(data); // can parse

            ArrayTuple array_data = (ArrayTuple)data;
            Assert.AreEqual(13, array_data.Value().Count); //13
            Assert.AreEqual(11, data[10]["id"].Value<int>()); //11
        }
        [TestMethod()]
        public void ParseRealValueTest()
        {
            string json = File.ReadAllText("Test3.json");
            Trace.WriteLine(json);
            Trace.WriteLine("----------------------------");
            JsonReader r = new JsonReader();
            var data = r.Parse(json);
            Assert.IsNotNull(data); // can parse

            Assert.AreEqual(null, data["info"].Value()); //null
            Assert.AreEqual(true, data.Value<bool>()); //true
        }
        [TestMethod()]
        public void ParseNullTest()
        {
            string json = File.ReadAllText("Test4.json");
            Trace.WriteLine(json);
            Trace.WriteLine("----------------------------");
            JsonReader r = new JsonReader();
            var data = r.Parse(json);
            Assert.IsNotNull(data); // can parse

            Assert.AreEqual(null, data[0].Value());  //null
            Assert.AreEqual(null, data["id"]["id"]["id"].Value()); //null
            Assert.AreEqual(null, data.Value()); //null
        }
        [TestMethod()]
        public void ParseErrorTest()
        {
            string json = File.ReadAllText("Test5.json");
            Trace.WriteLine(json);
            Trace.WriteLine("----------------------------");
            JsonReader r = new JsonReader();
            var data = r.Parse(json);
            Assert.IsNull(data); // can not parse
        }
    }
}