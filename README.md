# JsonReader

ZapLib 2 的實驗模組，將 Json 資料轉換成允許使用 Indexer 索引的方式在 C# 中方便的存取

## Usage example

以下為實際示範案例

### Normal JSON

```json
{
  "name": "zap",
  "age": 99,
  "permission": [ 1, 2, 3, 4, 5 ],
  "is_admin": true,
  "since": "2019-04-13T18:52:27.526903+08:00",
  "info": {
    "city": "USA",
    "off": 0.5,
    "product": ["apple","banana"],
    "tel": null,
    "logo": {
      "host": {},
      "work": {
        "good": {
          "url": "http://www.google.com"
        }
      }
    }
  }
}
```

**example**

```csharp
string json = File.ReadAllText("Test1.json"); 
JsonReader r = new JsonReader();
var data = r.Parse(json);

string val1 = data["info"]["logo"]["work"]["good"]["url"].Value<string>(); //http://www.google.com
string val2 = data["info"]["product"][0].Value<string>(); //apple
```


### Array JSON

```json
[
  {
    "id": 0
  }
]
```

**example**

```csharp
string json = File.ReadAllText("Test2.json");
JsonReader r = new JsonReader();
var data = r.Parse(json);

ArrayTuple array_data = (ArrayTuple)data;
var val1 = array_data.Value().Count; //1
var val2 = data[0]["id"].Value<int>(); //0
```

### Real-Value JSON

```json
true
```

**example**

```csharp
string json = File.ReadAllText("Test3.json");
JsonReader r = new JsonReader();
var data = r.Parse(json);

var val1 = data["info"].Value(); //null
var val1 = data.Value<bool>(); //true
```


### NULL JSON

```json
null
```

**example**

```csharp
string json = File.ReadAllText("Test4.json");
JsonReader r = new JsonReader();
var data = r.Parse(json);

var val1 = data[0].Value();  //null
var val2 = data["id"]["id"]["id"].Value(); //null
var val3 = data.Value(); //null
```

### IT IS NOT JSON

```json
<?xml version="1.0" encoding="utf-8" ?>
<catalog>
    <product>
        <pname>HP printer</pname>
        <price currency="NT">12000</price>
        <mark />
    </product>
    <hr />
    <product>
        <pname>IBM notebook</pname>
        <price currency="NT">4900</price>
        <mark>10Percent off</mark>
    </product>
</catalog>             
```

**example**

```csharp
string json = File.ReadAllText("Test5.json");
JsonReader r = new JsonReader();
var data = r.Parse(json); // null
Assert.IsNull(data); // can not parse
```

## License

   Copyright 2019 LinZap

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.